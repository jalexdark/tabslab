package com.george.tabslab.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.tabslab.R;
import com.george.tabslab.models.Person;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by STX-210916-1 on 06/12/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{

    private Context context;
    private List<Person> list;

    public RecyclerAdapter(Context context, List<Person> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person person = list.get(position);
        holder.personName.setText(person.getName());
        holder.countryName.setText(person.getCountry().getName());
        Picasso.with(context).load(person.getCountry().getFlagUrl()).into(holder.countryImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView countryImage;
        private TextView countryName;
        private TextView personName;

        public ViewHolder(View itemView) {
            super(itemView);
            countryImage = (ImageView) itemView.findViewById(R.id.card_country_image);
            countryName = (TextView) itemView.findViewById(R.id.card_country_name);
            personName = (TextView) itemView.findViewById(R.id.card_person_name);
        }
    }
}
