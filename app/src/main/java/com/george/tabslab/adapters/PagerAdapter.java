package com.george.tabslab.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.george.tabslab.fragments.FormFragment;
import com.george.tabslab.fragments.ListFragment;

/**
 * Created by STX-210916-1 on 06/12/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    private int fragments;

    public PagerAdapter(FragmentManager fm, int fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new FormFragment();

            case 1:
                return new ListFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return fragments;
    }
}
