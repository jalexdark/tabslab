package com.george.tabslab.interfaces;

import com.george.tabslab.models.Person;

/**
 * Created by STX-210916-1 on 06/12/2017.
 */

public interface OnPersonCreated {
    void createPerson(Person person);
}
