package com.george.tabslab.models;

/**
 * Created by STX-210916-1 on 06/12/2017.
 */

public class Country {

    private String name;
    private String countryCode;
    private final String url = "http://www.geognos.com/api/en/countries/flag/%1$s.png";

    public Country(String name, String countryCode) {
        this.name = name;
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getFlagUrl(){
        return String.format(url, countryCode);
    }

    @Override
    public String toString() {
        return name;
    }
}
