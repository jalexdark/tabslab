package com.george.tabslab.models;

/**
 * Created by STX-210916-1 on 06/12/2017.
 */

public class Person {

    private String name;
    private Country country;

    public Person(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public Country getCountry() {
        return country;
    }
}
