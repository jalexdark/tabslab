package com.george.tabslab.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.george.tabslab.R;
import com.george.tabslab.Utils.Util;
import com.george.tabslab.interfaces.OnPersonCreated;
import com.george.tabslab.models.Country;
import com.george.tabslab.models.Person;

import java.util.List;

import static com.george.tabslab.Utils.Util.shortToast;

public class FormFragment extends Fragment implements View.OnClickListener{

    private OnPersonCreated onPersonCreated;

    private View view;

    private EditText personName;
    private Spinner personCountry;
    private Button button;

    private List<Country> countries;

    public FormFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_form, container, false);
        initViews();
        initSpinner();
        setEvents();
        return view;
    }

    private void initViews() {
        personName = (EditText) view.findViewById(R.id.person_name);
        button = (Button) view.findViewById(R.id.agregar);
    }

    private void setEvents() {
        button.setOnClickListener(this);
    }

    private void initSpinner() {
        personCountry = (Spinner) view.findViewById(R.id.person_country);
        countries = Util.getCountries();

        ArrayAdapter<Country> countryArrayAdapter = new ArrayAdapter<Country>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);
        personCountry.setAdapter(countryArrayAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.agregar:
                createNewPerson();
                break;
        }
    }

    private void createNewPerson() {
        String name = personName.getText().toString();

        if (name.equals("")){
            shortToast(getContext(), getString(R.string.error_empty_name));
            return;
        }

        Country country = (Country) personCountry.getSelectedItem();
        Person person = new Person(name, country);
        onPersonCreated.createPerson(person);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPersonCreated){
            onPersonCreated = (OnPersonCreated)context;
        }else {
            throw new RuntimeException(context.toString() + " must implement OnPersonCreated");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPersonCreated = null;
    }
}
