package com.george.tabslab.fragments;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.george.tabslab.R;
import com.george.tabslab.adapters.RecyclerAdapter;
import com.george.tabslab.models.Person;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    private List<Person> list = new ArrayList<>();

    private View view;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);
        initRecycler();
        return view;
    }

    private void initRecycler() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        adapter = new RecyclerAdapter(getContext(), list);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void addPerson(Person person){
        list.add(person);
        adapter.notifyDataSetChanged();
    }

}
